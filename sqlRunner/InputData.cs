﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using excelTools.excelLoader;
using excelTools.utilities;

namespace sqlRunner
{
    class InputData
    {
        public string Reference { get; set; }
    }

    class InputDataBuilder : IDataBuilder<InputData>
    {
        InputData IDataBuilder<InputData>.Build(object[,] data, int row)
        {
            InputData inputData = new InputData();
            inputData.Reference = Helpers.GetValueFromCellTrimed(data, row, 0);
            return inputData;
        }
    }
}