﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using excelTools.excelLoader;
using MySql.Data;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace sqlRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo input = new FileInfo("input.xlsx");

            CheckStartignConditions(input, args);

            List<InputData> data = new ExcelFileLoader().GetData<InputData, InputDataBuilder>(input.FullName);
            Console.WriteLine(data.Count + " records loaded");
            Console.WriteLine("Executing query \"" + args[0] + "\"");

            ExecteQuery(data, args);
            Console.WriteLine("result.csv ready");
            Console.WriteLine("Opening result.csv");
            Process.Start("result.csv");
        }

        private static void ExecteQuery(List<InputData> data, string[] args)
        {
            using (MySqlConnection connection = GetConnection())
            using (StreamWriter writer = new StreamWriter("result.csv", false))
            {
                OpenConnection(connection);
                for (int i = 0; i < data.Count; i++)
                {
                    using (MySqlCommand commend = GetCommend(data[i], args, connection))
                    using (MySqlDataReader reader = commend.ExecuteReader())
                    {
                        SaveToFile(reader, writer, data[i]);
                        ReportProgress(i, data.Count);
                    }
                }
                Console.WriteLine("");
            }
        }


        private static void ReportProgress(int progress, int count)
        {
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write("Progress " + (progress + 1) + " / " + count);
        }

        private static bool firstRow = true;
        private static void SaveToFile(MySqlDataReader reader, StreamWriter writer, InputData inputData)
        {
            if (firstRow)
            {
                PrepareHeaders(reader, writer);
                firstRow = false;
            }

            while (reader.Read())
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    if (i < reader.FieldCount)
                        sb.Append(reader.GetValue(i).ToString() + ";");
                }

                sb.Append(inputData.Reference);
                writer.WriteLine(sb.ToString());
            }
        }

        private static void PrepareHeaders(MySqlDataReader reader, StreamWriter writer)
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (i < reader.FieldCount)
                    builder.Append(reader.GetName(i) + ";");
            }

            builder.Append("Original Reference");

            writer.WriteLine(builder.ToString());
        }

        private static MySqlCommand GetCommend(InputData inputData, string[] args, MySqlConnection connection)
        {
            string cmdString = string.Format(args[0], inputData.Reference);
            return new MySqlCommand(cmdString, connection);
        }

        private static void OpenConnection(MySqlConnection connection)
        {
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not open MySqlConneciton");
                throw e;
            }
        }

        private static MySqlConnection GetConnection()
        {
            var connectionInfo = JsonConvert.DeserializeObject<ConnectionInfo>(File.ReadAllText("connection.json"));
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
            builder.Database = connectionInfo.DBName;
            builder.Server = connectionInfo.Server;
            builder.UserID = connectionInfo.UserName;
            builder.Password = connectionInfo.Password;
            builder.Port = uint.Parse(connectionInfo.Port);
            return new MySqlConnection(builder.ToString());
        }

        private static void CheckStartignConditions(FileInfo input, string[] args)
        {
            if (args.Length == 0)
                MissingArgs();

            if (!input.Exists)
                FileDontExist();

            if (args[0].ToLower().Substring(0, 6).Contains("insert")
                || args[0].ToLower().Substring(0, 6).Contains("update")
                || args[0].ToLower().Substring(0, 6).Contains("delete"))
                RestrictedCommend();

        }


        private static void FileDontExist()
        {
            Console.WriteLine("Missing \"input.xlsx\"");
            Environment.Exit(0);
        }

        private static void MissingArgs()
        {
            Console.WriteLine("You did not have provided any arguments");
            Environment.Exit(0);
        }


        private static void RestrictedCommend()
        {
            Console.WriteLine("Can't execute INSERT, DELETE and UPDATE instructions.");
            Environment.Exit(0);
        }
    }
}